function initApp() {
    var earth = new WE.map('earth_div', {
        sky: true,
        stars: true
    });
    WE.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap contributors',
        minZoom: 0,
        maxZoom: 16,
    }).addTo(earth);

    WE.tileLayer('http://data.webglearth.com/natural-earth-color/{z}/{x}/{y}.jpg', {
        tileSize: 256,
        bounds: [[-85, -180], [85, 180]],
        minZoom: 0,
        maxZoom: 16,
        attribution: 'WebGLEarth',
        tms: true,
        opacity: 0.5
    }).addTo(earth);

    var marker = WE.marker([51.5, -0.1], 'iss.png', 100, 97).addTo(earth)
    // refresh iss pos
    setInterval(function () {
        iss.getData().then(function (result) {
            console.log(result);
            document.getElementById('info_panel').innerHTML = result.location;
            marker.setPosition(result.latitude, result.longitude);            
        }).catch(function (error) {
            console.error(error);
        });
    }, 5000);
}
