var iss = {
    getData: () => {
        let request = new XMLHttpRequest();
        request.open('GET', 'https://api.wheretheiss.at/v1/satellites/25544', true);
        return new Promise((onSuccess, onError) => {
            request.onload = () => {
                if (request.status >= 200 && request.status < 400) {
                    var data = JSON.parse(request.responseText);
                    iss.getLocFromLL(data.latitude, data.longitude).then(result => {
                        data.location = result;
                        onSuccess(data);
                    }).catch(error => {
                        data.location = 'Brak danych';
                        onSuccess(data); // i tak puszczamy mimo ze google sie wykrzaczylo
                    })
                } else {
                    onError(request.statusText);
                }
            };

            request.onerror = function () {
                onError(false);
            };

            request.send();
        })
    },

    /**
     * Lat,Lng to Location
     */
    getLocFromLL: (lat, lng) => {
        let gLL = new google.maps.LatLng(lat, lng);
        let gc = new google.maps.Geocoder();
        let req = {
            "location": gLL
        };
        return new Promise((onSuccess, onError) => {
            gc.geocode(req, (results, status) => {
                console.log(req, results, status);
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        onSuccess(results[0].formatted_address);
                    };
                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    onSuccess('Brak lokalizacji');
                } else {
                    onError(status);
                }
            })
        });
    }
}