import React from 'react';
import { render } from 'react-dom';


export default class InfoPanel extends React.Component {

    findMe() {
        this.props.map.setView([this.props.data.latitude, this.props.data.longitude], 5);
    }

    render() {
        if (this.props.data !== undefined) {
            return (
                <div className="InfoPanel">
                    <ul>
                        <li>Latitude: {this.props.data.latitude.toFixed(4)}</li>
                        <li>Longitude: {this.props.data.longitude.toFixed(4)}</li>
                        <li>Wysokość: {this.props.data.altitude.toFixed(2)} km</li>
                        <li>Prędkość: {this.props.data.velocity.toFixed(2)} km/h</li>
                        <li>Data: {this.props.data.stamp}</li>
                    </ul>
                    <div>
                        <input type="button" value="Znajdź na mapie" onClick={this.findMe.bind(this)} />
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }
};