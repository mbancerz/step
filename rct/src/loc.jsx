import React from 'react';
import { render } from 'react-dom';

export default class LocPanel extends React.Component {

    state = {
        location: 'Brak danych o lokalizacji'
    }

    getLocation(lat, lng) {
        let gLL = new google.maps.LatLng(lat, lng);
        let gc = new google.maps.Geocoder();
        let req = {
            "location": gLL
        };
        return new Promise((onSuccess, onError) => {
            gc.geocode(req, (results, status) => {
                // console.log(req, results, status);
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        onSuccess(results[0].formatted_address);
                    };
                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    onSuccess('Brak lokalizacji');
                } else {
                    onError(status);
                }
            })
        });
    }

    componentWillReceiveProps(next) {
        this.getLocation(next.data.latitude, next.data.longitude).then(location => {
            this.setState({ location: location })
        }).catch(error => {
            this.setState({ location: error})
        })
    }

    render() {
        if (this.props.data !== undefined) {
            return (
                <div className="LocPanel">{this.state.location}</div>
            )
        } else {
            return null;
        }
    }
};