import React from 'react';
import { render } from 'react-dom';
import Leaflet from 'leaflet';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import { extend } from 'lodash'
import InfoPanel from './info.jsx';
import LocPanel from './loc.jsx';
import moment from 'moment';
moment.locale('pl');
Leaflet.Icon.Default.imagePath = '//cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.0/images/'

export default class Mapa extends React.Component {

    state = {
        marker: {
            position: [51.505, -0.09],
            data: null
        },
        map: {
            center: [51.505, -0.09],
            zoom: 13
        }
    }

    constructor() {
        super();
    }

    componentDidMount() {
        var self = this;
        setInterval(() => {
            var request = new Request('https://api.wheretheiss.at/v1/satellites/25544');
            fetch(request).then(response => {
                response.json().then(data => {
                    data.stamp = moment.unix(data.timestamp).format("YYYY/MM/DD HH:mm:ss:SS");
                    var newMarker = extend({}, self.state.marker);
                    newMarker.position = [data.latitude, data.longitude];
                    newMarker.data = data;
                    self.setState({ marker: newMarker });
                })
            }).catch(error => {
                console.error('Data fetch failed', error);
            })
        }, 5000);
    }

    render() {
        return (
            <Map center={this.state.map.center} zoom={this.state.map.zoom}>
                <LocPanel data={this.state.marker.data || undefined}></LocPanel>
                <InfoPanel data={this.state.marker.data || undefined}></InfoPanel>
                <TileLayer
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' />
                <Marker position={this.state.marker.position}>
                    <Popup>
                        <span>A pretty CSS3 popup.<br />Easily customizable.</span>
                    </Popup>
                </Marker>
            </Map>
        )
    }
};